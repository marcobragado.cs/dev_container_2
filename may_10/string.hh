#include <string.h>
#include <iostream>
#include <memory>
#include <cstring>

// String library
class string
{
    std::unique_ptr<char[]> m_string;
    int m_max_length;

    // Bonus points if you use unique_pointer
    // std::unique_ptr<char> m_string;

public:
    string(int max_length)
    {
        m_max_length = max_length + 1; // +1 for NULL termination
        m_string = std::make_unique<char[]>(max_length);
    }

    // Rule of 3: Because we will allocate memory dynamically
    /*
        Using a unique_pointer so no destructor needed.

         ~string() {
        delete [] m_string;
    };
    */

    // Implement constructor to allocate as much memory as the c_string
    string::string(const char *c_string)
    {
        m_max_length = std::strlen(c_string);
        m_string = std::make_unique<char[]>(m_max_length);
        std::strcpy(m_string.get(), c_string);
    };

    // Rule of 3: 2) Implement the copy constructor
    string::string(const string &copy)
    {
        m_max_length = copy.m_max_length;
        m_string = std::make_unique<char[]>(m_max_length);
        std::strcpy(m_string.get(), copy.m_string.get());
    };

    // Rule of 3: 3) Implmement the assignment operator
    string &string::operator=(const string &source)
    {
        if (this == &source)
            return *this;

        m_max_length = source.m_max_length;
        m_string = std::make_unique<char[]>(m_max_length);
        std::strcpy(m_string.get(), source.m_string.get());

        return *this;
    };

    // Mutable API to make all characters lowercase or uppercase
    void string::to_upper()
    {
        for (size_t i = 0; i < m_max_length; ++i)
        {
            m_string[i] = std::toupper(m_string[i]);
        }
    };

    void string::to_lower()
    {
        for (size_t i = 0; i < m_max_length; i++)
        {
            m_string[i] = std::tolower(m_string[i]);
        }
    };

    // Adding more data to the string
    void string::append_char(char c) // Append a char but only if string has memory available
    {
        // get length of m_string
        size_t current_m_string_length = std::strlen(m_string.get());

        // check if m_string arr has space
        if (current_m_string_length < m_max_length - 1)
        {
            // replace null terminator with input char
            m_string[current_m_string_length] = c;

            // set last index to null terminator
            m_string[current_m_string_length + 1] = '\0';
        }
    };

    void string::append_string(const char *c_string) // Append another c_string only if string has memory available
    {
        size_t current_length = std::strlen(m_string.get());
        size_t append_length = std::strlen(c_string);

        if (current_length + append_length < m_max_length)
        {
            for (size_t i = 0; i < append_length; i++)
            {
                m_string[current_length + i] = c_string[i];
            }
            m_string[current_length + append_length] = '\0';
        }
    };

    // Non-mutable APIs to check certain properties of the string
    bool string::equals_to(const char *c_string)
    {
        return std::strcmp(m_string.get(), c_string) == 0;
    };

    bool string::contains(const char *c_string)
    {
        size_t current_length = std::strlen(m_string.get());

        for (size_t i = 0; i < std::strlen(c_string); i++)
        {
            char current_char = c_string[i];
            if (current_char == *c_string)
            {
                return true;
            }
        }
    };
    bool string::begins_with(const char *c_string)
    {
        size_t c_string_length = std::strlen(c_string);
        return std::strncmp(m_string.get(), c_string, c_string_length) == 0;
    };

    int get_length()
    {
        return std::strlen(m_string.get());
    };

    void print() { std::cout << "String is: '" << m_string.get() << "'" << std::endl; }

    // Other mutable APIs
    void string::clear()
    {
        m_string = std::make_unique<char[]>(1);
        m_string[0] = '\0';
    };

    void set(const char *string)
    {
        strncpy(m_string.get(), string, m_max_length);

        // All 3 lines do the same thing
        m_string[m_max_length - 1] = '\0';
        m_string[m_max_length - 1] = 0;
        // m_string[m_max_length - 1] = NULL;

        // We need the line(s) above because in case strncpy() ran out of space, it won't null terminate

        // "hello" -> 6 spaces
        // [0] = h, [1] = e, [2] = l, [3] = l, [4] = o, [5] = '\0';
    }
};
